rmdir /s /q built
docker build . -f builders/windows/Dockerfile -t slice-editor-windows --build-arg DEBUG="-debug"
if %errorlevel% neq 0 exit /b %errorlevel%
docker create --name slice-editor-windows slice-editor-windows
docker cp slice-editor-windows:/game/built built
docker rm slice-editor-windows
cd built
slice-editor.exe
