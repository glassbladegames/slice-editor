#!/bin/sh
set -eux
rm -rf built
docker build . -f builders/ubuntu/Dockerfile -t slice-editor-ubuntu --build-arg DEBUG="-debug"
docker create --name slice-editor-ubuntu slice-editor-ubuntu
docker cp slice-editor-ubuntu:/game/built built
docker rm slice-editor-ubuntu
cd built
./slice-editor
