#include <slice3d.h>
#include <camcontrol.h>

s3dRenderCamera* main_cam;
s3dLight* main_light;
struct RenderTargets
{
    slBox* boxL;
    slBox* boxR;
    slScalar scaling = 1;//0.125;
    //slVec2 boxsize = slVec2(0.5,1);
    slVec2 boxsize = slVec2(1); // depth box shoved offscreen

    s3dObject* sprite_obj;
    slScalar elapsed = 0;
    s3dLight* cam_light = s3dLight::Create();
    s3dRenderCamera cam_camera;

    RenderTargets ()
    {
        cam_camera.Resize(slInt2(384));

        boxL = slCreateBox(main_cam->GetColor());
        boxL->SetDims(slVec2(0,0),boxsize,255);
        boxR = slCreateBox(main_cam->GetDepth());
        boxR->SetDims(slVec2(boxsize.x,0),boxsize,255);

        s3dModelRef sprite_model = s3dSpriteModel("doesn't exist",true,1,0,1,120,true);

        //sprite_model->SetSpriteTex(frame);
        s3dMesh* mesh = sprite_model->meshes + 0;
        slTexture* junk_ambient = mesh->ambient_texref;
        slTexture* junk_diffuse = mesh->diffuse_texref;
        mesh->ambient_texref = cam_camera.GetColor().ReserveGetRaw();
        mesh->diffuse_texref = cam_camera.GetColor().ReserveGetRaw();
        if (junk_ambient) junk_ambient->Abandon();
        if (junk_diffuse) junk_diffuse->Abandon();

        sprite_obj = s3dCreateObject(sprite_model);
        sprite_obj->SetWorldTransform(GLmat4_offset(11,6.32,5.5) * GLmat4_yrotate(slDegToRad(180)) * GLmat4_scaling(GLvec4(2)));

        cam_light->color = GLcolor3(1,0,0);
        cam_light->strength = 8;
        cam_light->SetCoverageAngle(50);

        cam_camera.SetOrigin(s3dVec3(12.08,3.42,4.93));
        cam_camera.SetPitch(-20);
        cam_camera.Perspective(50,0.1,200);
        *cam_light->GetCamera() = cam_camera;

        main_light->SetShadowRes(1024);
    }
    ~RenderTargets ()
    {
        s3dLight::Destroy(cam_light);
        slDestroyBox(boxL);
        slDestroyBox(boxR);
        s3dDestroyObject(sprite_obj);
    }
    void Draw ()
    {
        s3dUpdateShadows();

        elapsed = slfmod(elapsed + slGetDelta() / 20,1);

        cam_camera.SetYaw(-45 + sin(elapsed*M_PI*2) * 30);
        cam_light->GetCamera()->SetYaw(cam_camera.GetYaw());
        cam_camera.Render();

        glBindTexture(GL_TEXTURE_2D,cam_camera.GetColor()->tex);
        glGenerateMipmap(GL_TEXTURE_2D);

        main_cam->Resize(boxsize * scaling);
        main_cam->Render();
    }
};
RenderTargets* render_targets;

void DrawEditor ()
{
    render_targets->Draw();

    slDrawToScreen();
    glFinish();
	slDrawUI();
    glFinish();
}

slKeyBind* move_light;
void MoveLight ()
{
    *main_light->GetCamera() = *main_cam;
    main_light->SetCoverageAngle(90);
    //main_light->GetCamera()->projection_matrix = GLmat4_ortho(-1,1,-1,1,0,10);
}

slKeyBind* toggle_fog;
bool fog_enabled = false;
void ToggleFog ()
{
    fog_enabled = !fog_enabled;
    s3dSetDistanceFog(fog_enabled,0,0.1);
}

slKeyBind* say_pos;
void SayPosition ()
{
    s3dVec3 pos = main_cam->GetOrigin();
    printf("Camera Position: %lf, %lf, %lf\n",pos.x,pos.y,pos.z);
}

struct EditingModel
{
    s3dModelRef model;
    s3dObject* object;
    EditingModel () : model(s3dSpriteModel("unloadable"))
    {
        object = s3dCreateObject(model);
        object->SetWorldTransform(GLmat4_identity());
    }
    ~EditingModel ()
    {
        s3dDestroyObject(object);
    }
};
EditingModel* CurrentlyEditingModel;

void EditorInit ()
{
    s3dInit();
    slSetAppDrawStage(DrawEditor);

    CurrentlyEditingModel = new EditingModel;

    main_light = s3dLight::Create();
    main_cam = new s3dRenderCamera;
    main_cam->Perspective(90,0.1,1000);
    //s3dSetShadowBias(0.1,1000);
    //s3dSetDistanceFog(true);

    main_cam->SetOrigin(s3dVec3(0,0,1));

    move_light = slGetKeyBind("Set Light Position", slKeyCode(SDLK_SPACE));
    move_light->onpress = MoveLight;
    MoveLight();

    toggle_fog = slGetKeyBind("Toggle Distance Fog", slKeyCode(SDLK_f));
    toggle_fog->onpress = ToggleFog;

    say_pos = slGetKeyBind("Say Camera Position", slKeyCode(SDLK_l));
    say_pos->onpress = SayPosition;

    s3dSetEnvironmentAmbientColor(GLcolor3(0.05,0.05,0.05));
    main_light->color = GLcolor3(0.5,0,1);
    main_light->strength = 30;



    render_targets = new RenderTargets;
}

void EditorQuit ()
{
    delete CurrentlyEditingModel;

    delete render_targets;

    move_light->onpress = NULL;
    toggle_fog->onpress = NULL;
    say_pos->onpress = NULL;

    delete main_cam;
    s3dLight::Destroy(main_light);

    s3dQuit();
    slSetAppDrawStage(NULL);
}

slScalar elapsed = 0;

void EditorStep ()
{
    elapsed = slfmod(elapsed + slGetDelta(), 4);
    //sprite->world_transform = GLmat4_yrotate(slDegToRad((int)(elapsed*2) * 90)) * GLmat4_offset(0,0,0.5);
    slScalar cam_yaw = (int)elapsed * 90 - 30;
    //s3dSetCamRotations(&cam_yaw,NULL,NULL);

    MoveLight();
}

void Editor ()
{
	EditorInit();
    CamMovementInit();
	while (!slGetReqt())
	{
        CamMovementUpdate();
    	EditorStep();
		slCycle();
	}
    CamMovementQuit();
	EditorQuit();
}
