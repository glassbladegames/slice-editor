#include <slice.h>
#include <sliceopts.h>
#include <slicefps.h>
#include <slice2d.h>

#include <editor.h>

int main ()
{
	slInit("Slice Editor");
	opInit();
	fpsInit();

	while (true)
	{
		Editor();
		// Only exit if the user asked to exit. Otherwise restart the game.
		if (slGetReqt()) break;
	};

	fpsQuit();
	opQuit();
	slQuit();
};
