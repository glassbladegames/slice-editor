#include <slice.h>
#include <slice3d.h>
#include <sliceopts.h>

slKeyBind* MoveForward;
slKeyBind* MoveBackward;
slKeyBind* MoveLeft;
slKeyBind* MoveRight;
slKeyBind* Run;
slKeyBind* MoveUp;
slKeyBind* MoveDown;

slScalar FreeCamFastSpeed = 20;
slScalar FreeCamNormalSpeed = 5;

extern s3dRenderCamera* main_cam;

void CamMovementUpdate ()
{
	s3dVec3 cam_inputs = s3dVec3(
		(MoveRight->down ? 1 : 0) - (MoveLeft->down ? 1 : 0),
		(MoveUp->down ? 1 : 0) - (MoveDown->down ? 1 : 0),
		(MoveBackward->down ? 1 : 0) - (MoveForward->down ? 1 : 0)
	);
    cam_inputs *=
          (Run->down ? FreeCamFastSpeed : FreeCamNormalSpeed)
        * slGetDelta()
    ;
	GLmat4 cam_input_rotation =
          GLmat4_yrotate(slDegToRad_F(main_cam->GetYaw()))
        * GLmat4_xrotate(slDegToRad_F(main_cam->GetPitch()))
    ;
	main_cam->SetOrigin(
          main_cam->GetOrigin()
        + s3dVec3(cam_input_rotation * cam_inputs)
    );
}

// To do: make mouse sensitivity adjustable in options menu.
slScalar HorizontalSensitivity = 0.05;
slScalar VerticalSensitivity = 0.05;
void OnInputEvent (SDL_Event event)
{
	if (event.type == SDL_MOUSEMOTION)
	{
		slScalar CamYaw = main_cam->GetYaw(), CamPitch = main_cam->GetPitch();
		CamYaw += event.motion.xrel * HorizontalSensitivity;
		CamPitch -= event.motion.yrel * VerticalSensitivity;
		CamPitch = slfmin(90,slfmax(-90,CamPitch));
        main_cam->SetYaw(CamYaw);
        main_cam->SetPitch(CamPitch);
	}
}

void OnOptsVisi (bool visi)
{
	// Purpose of this: if the user hits Escape and goes in the options menu, turn off relative mouse mode.
	if (visi) SDL_SetRelativeMouseMode(SDL_FALSE);
	else SDL_SetRelativeMouseMode(SDL_TRUE);
}

void CamMovementInit ()
{
	MoveLeft = slGetKeyBind("Move Left",slKeyCode(SDLK_a));
	MoveRight = slGetKeyBind("Move Right",slKeyCode(SDLK_d));
	MoveForward = slGetKeyBind("Move Forward",slKeyCode(SDLK_w));
	MoveBackward = slGetKeyBind("Move Backward",slKeyCode(SDLK_s));
	MoveUp = slGetKeyBind("Move Up",slKeyCode(SDLK_e));
	MoveDown = slGetKeyBind("Move Down",slKeyCode(SDLK_q));
	Run = slGetKeyBind("Run",slKeyCode(SDLK_LSHIFT));

	// Mouse input for camera movement.
	SDL_SetRelativeMouseMode(SDL_TRUE);
	slSetAllEventsHandler(OnInputEvent);
	opSetVisibilityCallback(OnOptsVisi);
}

void CamMovementQuit ()
{
	// there are no onpress callbacks to clear since none were set

	// Let go of mouse input.
	SDL_SetRelativeMouseMode(SDL_FALSE);
	slSetAllEventsHandler(NULL);
	opSetVisibilityCallback(NULL);
}
