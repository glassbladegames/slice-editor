ifeq ($(DEBUG),-debug)
CFLAGS += -w -g
else
CFLAGS += -O2 -fomit-frame-pointer -s
endif
CXXFLAGS = $(CFLAGS) -fpermissive

EDITOR_BINARY = built/slice-editor

INCLUDES += -Iinclude #-I/usr/local/include/ -I/usr/include/

LIBRARIES =\
	-lsliceopts\
	-lslicefps\
	\
	-lslice3d\
	-lbullet\
	\
	-lslice2d\
	-lbox2d \
	\
	-lslice\
	\
 	-lvorbisfile\
	-lvorbis\
	-logg\
	-lSDL2_image\
 	-lSDL2_ttf\
	-lSDL2\
	\
	$(OTHER_LIBRARIES)

#LIBDIR += -L /usr/local/lib/ -L /usr/lib/

EDITOR_SOURCES = $(shell find src -iname '*.cpp')
EDITOR_OBJS = $(patsubst %.cpp,%.o,$(EDITOR_SOURCES))

#WINDRES_SOURCES will be set by the calling Dockerfile if we're on Windows.
WINDRES_OBJS = $(patsubst %.rc,%.res,$(WINDRES_SOURCES))

all: $(EDITOR_OBJS) $(WINDRES_OBJS)
	echo "WINDRES_SOURCES: ${WINDRES_SOURCES}"
	$(CXX) $(CFLAGS) -o $(EDITOR_BINARY) $(INCLUDES) $(LIBDIR) $(EDITOR_OBJS) $(WINDRES_OBJS) $(LIBRARIES)

clean:
	@rm -rvf $(shell find . -iname '*.o')
	@rm -rvf $(shell find . -iname '*.res')

%.o: %.cpp
	$(CXX) -c $(CXXFLAGS) $(INCLUDES) $< -o $@

%.res: %.rc
	echo "doing resource compile"
	x86_64-w64-mingw32-windres $< -O coff -o $@
